import {Component, OnInit} from '@angular/core';
import {User} from "../user";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../user.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  user: User;
  public userForm: FormGroup = null;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder,
              private userService: UserService) {
    this.user = new User();
  }

  get loginField() {
    return this.userForm.get('userLogin')
  }

  get emailField() {
    return this.userForm.get('email')
  }

  get nameField() {
    return this.userForm.get('userName')
  }

  get surnameField() {
    return this.userForm.get('userSurname')
  }

  get passwordField() {
    return this.userForm.get('userPassword')
  }

  get ageField() {
    return this.userForm.get('age')
  }

  onSubmit() {
    this.createObject();
    this.userService.save(this.user).subscribe(result => this.gotoUserList());
  }

  gotoUserList() {
    this.router.navigate(['/users']);
  }


  private prepareForm() {
    this.userForm = this.formBuilder.group({
      userLogin: new FormControl(this.user.userLogin, Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ])),
      userName: new FormControl(this.user.userName, Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ])),
      userSurname: new FormControl(this.user.userSurname, Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ])),
      userPassword: new FormControl(this.user.userPassword, Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(255)
      ])),
      email: new FormControl(this.user.email, Validators.compose([
        Validators.required,
        Validators.email
      ])),
      age: new FormControl(this.user.age, Validators.compose(([
        Validators.required,
        Validators.min(13),
        Validators.max(120)
      ])))
    });
  }

  rand20User() {
    let nameList: string[] = ['Willetta', 'Beckie', 'Shenita', 'Ebonie', 'Particia', 'Donna', 'Denisse', 'Camille', 'Enrique', 'Noemi', 'Allegra', 'Ariana', 'Rhett', 'Alise', 'Tresa', 'Shirleen', 'Beaulah', 'Lasandra', 'Hope', 'Hopper'];
    for (let name of nameList) {
      let randomUser = new User();
      randomUser.setUser(name + "123", name, name.repeat(2).substring(3.8).toLowerCase(), name, name + "@gmail.com", Math.floor(Math.random() * 12) + 18);
      this.userService.save(randomUser).subscribe(result => this.gotoUserList());
    }
  }

  createObject() {
    this.user.userLogin = this.loginField.value;
    this.user.userName = this.nameField.value;
    this.user.userSurname = this.surnameField.value;
    this.user.userPassword = this.passwordField.value;
    this.user.email = this.emailField.value;
    this.user.age = this.ageField.value;
  }


  ngOnInit() {
    this.prepareForm();
  }
}

