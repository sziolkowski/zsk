import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../user.service";
import {User} from "../user";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {md5} from "../../resources/md5";


@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  public user: User = null;
  public userForm: FormGroup = null;


  constructor(private activatedRoute: ActivatedRoute,
              private userService: UserService,
              private formBuilder: FormBuilder,
              private router: Router) {
  }

  get loginField() {
    return this.userForm.get('login')
  }

  get emailField() {
    return this.userForm.get('email')
  }

  get nameField() {
    return this.userForm.get('userName')
  }

  get surnameField() {
    return this.userForm.get('userSurname')
  }

  get passwordField() {
    return this.userForm.get('password')
  }

  get ageField() {
    return this.userForm.get('age')
  }


  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      const userId = +params['id'];
      this.loadUser(userId);
    })
  }

  private loadUser(userId) {
    this.userService.findAll().subscribe(users => {
      users.filter(it => it.id === userId).forEach(user => {
        this.user = user;
        user.userPassword = md5(user.userPassword);
        this.prepareForm();
      })
    });
  }

  private prepareForm() {
    this.userForm = this.formBuilder.group({
      userLogin: new FormControl(this.user.userLogin, Validators.compose([
      ])),
      userName: new FormControl(this.user.userName, Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ])),
      userSurname: new FormControl(this.user.userSurname, Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(255)
      ])),
      userPassword: new FormControl(this.user.userPassword + " " , Validators.compose([
      ])),
      email: new FormControl(this.user.email, Validators.compose([
        Validators.required,
        Validators.email
      ])),
      age: new FormControl(this.user.age, Validators.compose(([
        Validators.required,
        Validators.min(13),
        Validators.max(120)
      ])))
    });
  }

  onSubmit() {
    this.user = Object.assign({}, this.user, this.userForm.value);
    this.userService.save(this.user).subscribe(user => {
      this.router.navigateByUrl('/users');
    })
  }

  randomEmail() {
    this.emailField.setValue('whatever@gmail.com');
    this.userForm.get('userName').disable();
  }


  backtoList() {
    this.router.navigateByUrl('/users');
  }
}
