import { Component, OnInit } from '@angular/core';
import {User} from "../user";
import {UserService} from "../user.service";
import {md5} from "../../resources/md5";
import {Router} from "@angular/router";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {

  users: User[];
  ageq: any;
  loginq: string;
  userNameq: any;
  idq: any;
  userSurnameq: any;
  emailq: any;

  constructor(private userService: UserService,
              private router: Router) {
  }

  hashPassword() {
    this.users.forEach(function (user) {
      user.userPassword = md5(user.userPassword);
    })
  }

  deleteUser(id: number) {
    this.userService.deleteUser(id).subscribe(user => {
      this.ngOnInit()
    })
  }

  ngOnInit() {
    this.userService.findAll().subscribe(data => {
      this.users = data;
      this.hashPassword()

    });
  }

  sortBy(type: string, variable: string) {
    if (type == "ASC"){
      this.users = this.users.sort((obj1, obj2) => {
        if (obj1[variable] > obj2[variable]) return 1;
        if (obj1[variable] < obj2[variable]) return -1;
        return 0;
      });
    }
    else if( type == "DESC"){
      this.users = this.users.sort((obj1, obj2) => {
        if (obj1[variable] < obj2[variable]) return 1;
        if (obj1[variable] > obj2[variable]) return -1;
        return 0;
      });
    }
  }
  cleanFilter() {
    this.idq = "";
    this.loginq = "";
    this.userNameq = "";
    this.userSurnameq = "";
    this.emailq = "";
    this.ageq = "";
  }
}
