package com.github.sziolkowski.polon;

import com.github.sziolkowski.polon.model.User;
import com.github.sziolkowski.polon.repo.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Random;
import java.util.stream.Stream;


@SpringBootApplication
public class PolonApplication {

	public static void main(String[] args) {
		SpringApplication.run(PolonApplication.class, args);
	}

	@Bean
	CommandLineRunner init(UserRepository userRepository) {
		return args -> {
			Stream.of("John", "Julie", "Jennifer", "Helen", "Rachel").forEach(name -> {
				User user = new User(name + "123",name, new StringBuilder(name).reverse().toString().toLowerCase(), "test",name.toLowerCase() + "@gmail.com", new Random().nextInt(10)+18, true);
				userRepository.save(user);
			});
			userRepository.findAll().forEach(System.out::println);
		};
	}
}